%MATLAB 2020b
%name: damping
%author: jakugre
%date: 2020-12-18
%version: v1.2

clear; 
%Set recording time, how many samples per second should the script take and
%surrounding's noise level
recording_time = 5;
sampling_frequency = 44100;
noise_level = 0.1;

%Create recorder object
recording_object = audiorecorder(sampling_frequency,16,1);

disp('Press to start');
pause()
pause(2)

disp('recording started')
recordblocking(recording_object, recording_time);
disp('Recording finished')
disp('------')
disp('Press any key to analyse and find damping coefficient')
pause;

%Get data from audio recorder
data = getaudiodata(recording_object);

%Create array for values that are over 0 so it is easier to deal with data
plus_data = [];


%Find average (mean) value of the data 
x = mean(data);


for i = [1:length(data)]
    %Scale data so it oscillates around 0
    data(i) = data(i) - x;
    if data(i) > 0
        plus_data = [plus_data,data(i)];
    end
    
end

%Array to help create plot
num = [];

for k = [1:length(plus_data)]
    num = [num,k];
end

max_value = max(plus_data);


    





    
index = find(plus_data == max_value);

tops = [];

value = max_value;


 
corrected_data = plus_data(index:end);



num_1 = [];
l = 1;

range = length(corrected_data)-49;
for z = [1:20:range]
    temp_data = corrected_data(z:z+49);
    temp_top = max(temp_data);
    tops = [tops,temp_top];
    num_1 = [num_1,l];
    l = l + 1;
end

%Array of indexes of next frequencies that match the equation A0/A = e
index1 = [];   

%Array of amplitudes
amplitudes = [];

for a = [3:length(plus_data)-2]
    %Check if a certain value is an amplitude (is top of certain part of
    %chart - maximum value in some range)
    if plus_data(a) == max(plus_data(a-2:a+2))
        amplitudes = [amplitudes, plus_data(a)];
    end
end


max_value_amplitudes = max(amplitudes);
index_of_max_amplitudes = find(amplitudes == max_value_amplitudes);
amplitudes_corrected = amplitudes(index_of_max_amplitudes:end);

%Search for amplitudes that match the equation A0/A = e, or, in this case
%A0/A - e < 0.1 - because results are not that accurate so there can be a
%slight mistake - 0.1 is chosen value, if set smaller then script will be more
%accurate
for p = [1:length(amplitudes_corrected)]
    temp = max_value/amplitudes_corrected(p);
    if abs(temp - exp(1)) < 0.1
        %Add index of matched values to array
        index1 = [index1,find(data == amplitudes_corrected(p), 1)];
        continue            
    end
    continue
end

%Choose first index, because next ones might be from different signals
useful_index = index1(1);
index_max = find(data == max_value);
if useful_index ~= 0
    
    time = (useful_index - index_max)/sampling_frequency;
    if time < 0
        disp('Could not find damping coefficient, please try again')
    else
        damp_coefficient = 1/time;
    end
else
    disp('Could not find damping coefficient, please try again')
end


list = [];

for w = [1:length(amplitudes_corrected)]
    list = [list,w];
end
    

   
% FFT analysis
analysis = fft(data);
sample_size = recording_object.TotalSamples;
sampling = recording_object.SampleRate;
power_spectrum = analysis.*conj(analysis)/sample_size;
fr = sampling*(1:sample_size)/sample_size;

len = [];
for i = [1:length(fr)]
    len = [len,i];
end



% Look for detected frequencies 
clear('frequencies');
noise_level = 0.1;
frequencies(1)=0;
n=1;

for k = 1:0.5*max(fr);
    if power_spectrum(k)>noise_level
        frequencies(n) = fr(k);
        n= n+1;
    end;
end;

dominant = fr(find(power_spectrum(1:0.5*max(fr))==max(power_spectrum(1:0.5*max(fr)))));
disp(['Dominant frequency is: ', num2str(dominant), ' Hz'])


    
  
analysis_data = fft(data);


subplot(3,1,1)
plot(data); 
xlabel("sample");
ylabel("frequency");

subplot(3,1,2)
plot(plus_data);
xlabel("sample of data greater than 0");
ylabel("frequency")

subplot(3,1,3)
plot(fr,power_spectrum);
title("FFT analysis")
xlabel("Frequency, Hz")
ylabel("PSD, m²/Hz")


